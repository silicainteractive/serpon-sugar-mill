﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AvatarImageScript : MonoBehaviour {
    public int playerID;
    public Image[] colorableSprites;
    public Image avatar;

    public void Awake() {
        var key = "Player" + playerID + "Color";
        if (!PlayerPrefs.HasKey(key)) return;
        var color = PlayerColors.Instance.ColorList[PlayerPrefs.GetString(key)];
        foreach (var sp in colorableSprites) {
            sp.color = color;
        }
        avatar.color = Color.white;
        
    }
}
