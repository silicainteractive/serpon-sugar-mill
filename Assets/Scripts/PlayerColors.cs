﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerColors : MonoBehaviour
{

    public string[] Keys;
    public Color[] Values;

    public Dictionary<string, Color> ColorList = new Dictionary<string, Color>();

    private static PlayerColors _instance;

    public static PlayerColors Instance
    {
        get
        {
            return _instance ?? (_instance = FindObjectOfType<PlayerColors>());
        }
    }


    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        for (int i = 0; i < Keys.Length; i++)
        {
            ColorList.Add(Keys[i], Values[i]);
        }
    }

    void Start()
    {
        ColorList.Add(Color.red.ToString(), Color.red);
        ColorList.Add(Color.green.ToString(), Color.green);
        ColorList.Add(Color.blue.ToString(), Color.blue);
        ColorList.Add(Color.cyan.ToString(), Color.cyan);
        ColorList.Add(Color.magenta.ToString(), Color.magenta);
        ColorList.Add(Color.yellow.ToString(), Color.yellow);
        ColorList.Add(Color.white.ToString(), Color.white);
        ColorList.Add(Color.grey.ToString(), Color.grey);
    }

}
