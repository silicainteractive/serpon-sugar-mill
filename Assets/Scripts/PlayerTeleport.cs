﻿using UnityEngine;
using System.Collections;

public class PlayerTeleport : MonoBehaviour {
    /// <summary>
    /// portal to teleport to
    /// </summary>
    public PlayerTeleport otherPortal;
    /// <summary>
    /// factors the teleportation over a 2D axis
    /// </summary>
    public Vector2 factor = new Vector2(1, 0);

    public GameObject cameThrough = null;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag != "Player" && other.tag != "BSpell") return;
        if (other.tag == "BSpell") {
            if (cameThrough == other.gameObject)
                return;
            otherPortal.cameThrough = other.gameObject;
        }
        other.transform.position = otherPortal.transform.parent.position;
    }
}
