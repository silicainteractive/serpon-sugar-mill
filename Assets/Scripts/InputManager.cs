﻿using System;
using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
    public int playerID;
    public int foobar;
    public new Rigidbody2D rigidbody;
    public float movementspeed = 6;
    public float jumpheight = 2;
    Vector2 movement;
    private bool isGrounded;
    Transform groundCheck;
    GameObject aimcursor;
    public int maxSpeedX;
    public int maxSpeedY;
    public Vector2 aim;
    private bool triggerPressed = false;
    Vector3 direction;
    public ParticleSystem deadParti;

    public SpriteRenderer[] colorableSprites;

    public Animator animator;

    public SpellSystem spellSystem;

    public void Awake() {
        var key = "Player" + playerID + "Color";
        var color = PlayerColors.Instance.ColorList[PlayerPrefs.GetString(key)];
        foreach (var sp in colorableSprites) {
            sp.color = color;
        }
    }

    void Start() {
        groundCheck = gameObject.transform.FindChild("GroundCheck");
        aimcursor = GameObject.FindGameObjectWithTag("Aim" + playerID);
    }

    void Update() {
        animator.SetFloat("Speed", Math.Abs(movement.x));
        animator.SetFloat("velocity Y", movement.y);
        if (Input.GetButtonDown("A" + playerID)) {
            spellSystem.ReceiveInput(SpellType.A);
        }
        if (Input.GetButtonDown("B" + playerID)) {
            spellSystem.ReceiveInput(SpellType.B);
        }
        if (Input.GetButtonDown("X" + playerID)) {
            spellSystem.ReceiveInput(SpellType.X);
        }
        if (Input.GetButtonDown("Y" + playerID)) {
            spellSystem.ReceiveInput(SpellType.Y);
        }

        //fire buttons
        if (!triggerPressed && Input.GetAxis("Trigger" + playerID) < 0) {
            triggerPressed = true;
            spellSystem.Cast(aimcursor.transform.localPosition);
            animator.SetTrigger("Attack");
        }
        if (triggerPressed && Input.GetAxis("Trigger" + playerID) >= 0) {
            triggerPressed = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        movement = new Vector2(0, 0);
        movement.x += movementspeed * Input.GetAxis("Horizontal" + playerID);
        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        animator.SetBool("Grounded", isGrounded);
        if (Math.Abs(Input.GetAxis("Horizontal" + playerID)) < 0) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x * 0.8f, rigidbody.velocity.y);
        }
        if (isGrounded && Input.GetAxis("Vertical" + playerID) < 0) {
            movement.y += foobar;
        }
        rigidbody.velocity += movement;

        //max movement speed
        movement = rigidbody.velocity;
        movement.x = Mathf.Clamp(movement.x, -maxSpeedX, maxSpeedX);
        movement.y = Mathf.Clamp(movement.y, -maxSpeedY, maxSpeedY);
        rigidbody.velocity = movement;
        
        //direction of aiming
        aim = new Vector2(Input.GetAxis("RightStickX" + playerID), Input.GetAxis("RightStickY" + playerID));
        if(Mathf.Abs(new Vector3(aim.x, aim.y * -1, 0).magnitude) > 0.2f)
        {
            direction = new Vector3(aim.x, aim.y * -1, 0);
        }
        aimcursor.transform.position = gameObject.transform.position + direction.normalized;
    }

    void OnDestroy()
    {
        Destroy(GameObject.FindGameObjectWithTag("Aim" + playerID));
    }
}
