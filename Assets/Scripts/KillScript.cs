﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillScript : MonoBehaviour {

    public int ownerId;
    void OnCollisionEnter2D(Collision2D col) {
        Kill(col.gameObject);
    }

    void OnTriggerEnter2D(Collider2D col) {
        Kill(col.gameObject);
    }

    public void Kill(GameObject go) {
        if (go.tag != "Player") return;

        Instantiate(go.GetComponent<InputManager>().deadParti, go.transform.position, go.transform.rotation);

        Destroy(go);
        ScoreBoard.Instance.AddKill(ownerId);
        Destroy(this.gameObject);
    }
}