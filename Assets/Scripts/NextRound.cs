﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NextRound : MonoBehaviour {
    public void Update() {
        if (Input.GetButtonDown("A1")) {
            SceneManager.LoadScene(2);
        }
        if (Input.GetButtonDown("B1")) {
            ScoreBoard.Instance.ClearScores();
            SceneManager.LoadScene(1);
        }
    }
}
