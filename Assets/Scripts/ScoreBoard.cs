﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {
    public int numberOfPlayers;

    public bool debugMode;

    public Text[] killLabels;

    public Dictionary<int, int> killCount;
    public SpawnPlayer[] spawnPoints; 
    private static ScoreBoard _instance;
    public static ScoreBoard Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<ScoreBoard>()); }
    }

    public event EventHandler OnKill;

    public void Awake() {
        killCount = new Dictionary<int, int>() {
            {1,0},
            {2,0},
            {3,0},
            {4,0}
        };
        GetScores();

        SetSpawnPoints();
        SetKillLabels();

    }

    private void SetKillLabels() {
        if (killLabels.Length <= 0) return;
        for (var i = 0; i < killCount.Count; i++) {
            killLabels[i].text = killCount[i+1].ToString();
        }
    }

    private void SetSpawnPoints() {
        if (spawnPoints.Length <= 0) return;
        numberOfPlayers = PlayerPrefs.GetInt("NumberOfPlayers");
        for (var i = 0; i < numberOfPlayers; i++) {
            spawnPoints[i].gameObject.SetActive(true);
        }
    }

    public void AddKill(int playerId) {
        Debug.Log("Kill awarded to player " + playerId);
        killCount[playerId]++;
        if (OnKill != null)
            OnKill(this, null);
    }

    public void SaveScores() {
        for (var i = 1; i <= numberOfPlayers; i++) {
            PlayerPrefs.SetInt(i.ToString(),killCount[i]);
        }
    }

    public void GetScores() {
        for (var i = 1; i <= numberOfPlayers; i++) {
            if (PlayerPrefs.HasKey(i.ToString()))
                killCount[i] = PlayerPrefs.GetInt(i.ToString());
        }
    }

    public void ClearScores() {
        PlayerPrefs.DeleteAll();
    }
}