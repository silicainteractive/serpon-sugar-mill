﻿using UnityEngine;
using System.Collections;

public class SpawnPlayer : MonoBehaviour {

    public GameObject player;
	// Use this for initialization
	void Start () {
        if (player != null)
        {
            GameObject spawnItem = Instantiate(player, transform.position, Quaternion.identity) as GameObject;
            spawnItem.name = player.name;
        }
        else
        {
            Debug.Log("no Player" + gameObject.name[11]);
        }
	}
}
