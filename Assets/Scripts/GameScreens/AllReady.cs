﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Linq;

public class AllReady : MonoBehaviour {

    private int amountOfPlayers = 4;
    private bool[] readyPlayers;

    void Start()
    {
        readyPlayers = new bool[amountOfPlayers];
    }
    /// <summary>
    /// sets the specified player on ready. Play is enabled when at least two players are ready
    /// </summary>
    /// <param name="player">index numbet of the player</param>
    public void SetReady(int player)
    {
        readyPlayers[player - 1] = !readyPlayers[player - 1];
    }
    /// <summary>
    /// counts the amount of players in ready state
    /// </summary>
    /// <returns></returns>
    private int CountReady() 
    {
        return readyPlayers.Count(b => b);
    }
    /// <summary>
    /// returns if it is possible to play with the current amount of players
    /// </summary>
    /// <returns></returns>
    private bool ReadyToPlay()
    {
        int amount = 0;
        amount = CountReady();

        return (amount >= 2);
    }

    private void Update() {
        //BEWARE: this is a debug settings
        if (!Input.GetKeyDown(KeyCode.Return)) return;
        readyPlayers[0] = true;
        readyPlayers[1] = true;
        Play();
    }

    /// <summary>
    /// runs check and enters playmode if possible
    /// </summary>
    public void Play()
    {
        if (!ReadyToPlay()) return;
        PlayerPrefs.SetInt("NumberOfPlayers", readyPlayers.Count(b => b));
        SceneManager.LoadScene(2);
    }
}
