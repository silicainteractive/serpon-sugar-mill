﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class PickCharacter : MonoBehaviour {

    private Image Image;
    private bool[] Available;
    private int index = 0;

    private float timer;
    private float timePlus = 0.25f;
    public HighlightButton play, menu, ready;
    public int playerId;

    public int Colour//Color mag niet :(
    {
        get { return index; }
        set
        {
            index = value;
            Image.color = PlayerColors.Instance.ColorList.Values.ElementAt(index);
            
        }
    }
    void Start()
    {
        Image = GetComponent<Image>();
        //Available = new bool[AmountOfColours];
        play = transform.parent.Find("Play").gameObject.GetComponent<HighlightButton>();
        menu = transform.parent.Find("Menu").gameObject.GetComponent<HighlightButton>();
        ready = gameObject.transform.Find(gameObject.name).GetComponent <HighlightButton>();
        
//      Add colors
       
        Colour = 0;
    }

    public void Up()
    {
        if (ready.IsSelected)
        {
            ready.FireButton();
        }

        if (Colour < PlayerColors.Instance.Keys.Length - 1)
        {
            Colour++;
        }
        else
        {
            Colour = 0;
        }
    }
    public void Down()
    {
        if (ready.IsSelected)
        {
            ready.FireButton();
        }

        if (Colour > 0)
        {
            Colour--;
        }
        else
        {
            Colour = PlayerColors.Instance.Keys.Length - 1;
        }
    }

    public void HandleInput()
    {
        string axis = "Vertical" + gameObject.name[gameObject.name.Length - 1];
        if (timer < Time.fixedTime)
        {
            if (Input.GetAxis(axis) > 0)
            {
                Up();
                timer = Time.fixedTime + timePlus;
            }
            else if (Input.GetAxis(axis) < 0)
            {
                Down();
                timer = Time.fixedTime + timePlus;
            }
            if (Input.GetAxis("Horizontal1") > 0)
            {
                menu.IsSelected = false;
                play.IsSelected = true;
            }
            else if (Input.GetAxis("Horizontal1") < 0)
            {
                menu.IsSelected = true;
                play.IsSelected = false;
            }
        }
        if (Input.GetButtonDown("A" + playerId))
        {
            ready.FireButton();
            PlayerPrefs.SetString("Player" + playerId + "Color", PlayerColors.Instance.ColorList.Keys.ElementAt(index));
        }
        if (Input.GetButtonDown("X1"))
        {
            if (play.IsSelected)
            {
                play.FireButton();
            }
            else if (menu.IsSelected)
            {
                menu.FireButton();
            }
        }

    }
    void Update()
    {
        HandleInput();
    }
}
