﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public void Load(int num)
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(num);
    }
}
