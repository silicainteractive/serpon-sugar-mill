﻿using UnityEngine;
using System.Collections;

public class ControllerButtons : MonoBehaviour
{
    private HighlightButton[] buttons;
    private HighlightButton currentButton;
    private int index = 0;
    private int maxIndex = 1;
    private float timer;
    float timePlus = 0.25f;

    public int Index
    {
        get { return index; }
        set
        {
            currentButton.FlipValue();
            index = value;
            currentButton = buttons[index];
            currentButton.FlipValue();
        }
    }
    void Start()
    {
        buttons = new HighlightButton[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            buttons[i] = transform.GetChild(i).GetComponent<HighlightButton>();
        }
        currentButton = buttons[Index];
        currentButton.FlipValue();
    }
    void HandleInput()
    {
        if (timer < Time.fixedTime)
        {
            if (Input.GetAxis("Vertical1") > 0)
            {
                if (Index < maxIndex)
                    Index++;
                else
                    Index = 0;
                timer = Time.fixedTime + timePlus;
            }
            else if (Input.GetAxis("Vertical1") < 0)
            {
                if (Index > 0)
                    Index--;
                else if (Index == 0)
                    Index = maxIndex;
                timer = Time.fixedTime + timePlus;
            }
        }
        if (Input.GetButtonDown("A1"))
        {
            currentButton.FireButton();
        }
    }
	
	// Update is called once per frame
	void Update () {
        HandleInput();
	}
}
