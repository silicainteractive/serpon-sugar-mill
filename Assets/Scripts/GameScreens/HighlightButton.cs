﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HighlightButton : MonoBehaviour {

    private bool highlight = false;
    private Button button;
    private Image image;

    public bool IsSelected
    {
        get { return highlight; }
        set { highlight = value;
            if (image == null)
            {
                image = GetComponent<Image>();
            }
            image.color = (value) ? Color.green : Color.white;
        }
    }
	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        Debug.Log(image);
        button = GetComponent<Button>();
	}

    public void FireButton()
    {
        var pointer = new PointerEventData(EventSystem.current);
        ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.pointerClickHandler);
    }
	// Update is called once per frame
	public void FlipValue () {
        IsSelected = !IsSelected;
	}
}
