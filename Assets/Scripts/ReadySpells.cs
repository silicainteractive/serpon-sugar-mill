﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReadySpells : MonoBehaviour {
    public SpriteRenderer[] spellSprites;
    public Dictionary<SpellType, Color> typeColors;
    private int currentSprite = 0;
    private Color baseColor = new Color(0,0,0,0);


    public void Awake() {
        typeColors = new Dictionary<SpellType, Color>() {
            {SpellType.A, Color.green },
            {SpellType.B, Color.red },
            {SpellType.X, Color.blue },
            {SpellType.Y, Color.yellow }
        };
    }

    public void Ready(SpellType type) {
        if (currentSprite >= spellSprites.Length) return;
        spellSprites[currentSprite++].color = typeColors[type];

    }

    public void Reset() {
        foreach (var sprite in spellSprites) {
            sprite.color = baseColor;
        }
        currentSprite = 0;
    }
}
