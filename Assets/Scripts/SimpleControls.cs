﻿using UnityEngine;
using System.Collections;

public class SimpleControls : MonoBehaviour {
    public float speed = 1;
    private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
	rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A))
            rb.AddForce(new Vector2(-speed, 0));
        if (Input.GetKey(KeyCode.D))
            rb.AddForce(new Vector2(speed, 0));
	}
}
