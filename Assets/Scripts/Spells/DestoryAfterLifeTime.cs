﻿using UnityEngine;
using System.Collections;

//Add this class to an object that needs to be destroyed after a fixed time
public class DestoryAfterLifeTime : MonoBehaviour {

    public float lifeTime;

	void Update () 
    {
        lifeTime -= Time.fixedDeltaTime;
        if (lifeTime < 0)
            Destroy(gameObject);
	}
}
