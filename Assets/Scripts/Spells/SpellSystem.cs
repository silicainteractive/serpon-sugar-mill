﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SpellSystem : MonoBehaviour {
    public Dictionary<SpellType, Type> availableSpells; 
    public Queue<Type> spellQueue;
    public ReadySpells spells;
    public void Awake() {
        availableSpells = new Dictionary<SpellType, Type>();
        availableSpells[SpellType.A] = typeof(ASpell);
        availableSpells[SpellType.B] = typeof(BSpell);
        availableSpells[SpellType.X] = typeof(XSpell);
        availableSpells[SpellType.Y] = typeof(YSpell);
        spellQueue = new Queue<Type>();
    }
    

    public void ReceiveInput(SpellType type) {
        if (spellQueue.Count > 5) return;
        spellQueue.Enqueue(availableSpells[type]);
        if(spells != null)
            spells.Ready(type);
    }

    public void Cast(Vector3 target) {
        if (spellQueue.Count == 0)
            return;

        var newTarget = target - transform.position;

        var spellType = spellQueue.Dequeue();
        AbstractSpell.CastSpellOfType(spellType, spellQueue, newTarget, transform, this);
        spellQueue.Clear();
        if (spells != null)
            spells.Reset();
    }
}

public enum SpellType {
    A,
    B,
    X,
    Y
}
