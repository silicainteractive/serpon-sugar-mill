﻿using UnityEngine;
using System.Collections;

public class MiniXSpell : AbstractSpell
{
    Animator anim;
    CircleCollider2D coll;

    public override float GrowScalar { get { return 0; } }

    public override float LifeTimeScalar { get { return 0; } }

    public override float MovementScalar { get { return 0; } }

    public override void Create(float growScalar, float movementScalar, float lifeTimeScalar)
    {
        base.Create(growScalar, movementScalar, lifeTimeScalar);

        movespeed = 4f;
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;

        coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.1f;
        coll.isTrigger = true;

        anim = gameObject.AddComponent<Animator>();
        sprRender = gameObject.AddComponent<SpriteRenderer>();

        gameObject.tag = "XSpell"; 

        sprRender.sprite = Resources.LoadAll<Sprite>("Sprites/XSpell")[0];
        sprRender.sortingLayerName = "Spells";
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("ACs/SpraySpellAOC");
        anim.runtimeAnimatorController = animAOC;
    }

    public override void Update()
    {
        if (!created)
            return;

        rb2d.velocity = movespeed * direction.normalized;
    }
}
