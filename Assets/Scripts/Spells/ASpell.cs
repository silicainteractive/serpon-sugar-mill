﻿using UnityEngine;
using System.Collections;
using System;

public class ASpell : AbstractSpell
{
    Animator anim;
    CircleCollider2D coll;

    public override float GrowScalar { get { return 1f; } }

    public override float LifeTimeScalar { get { return 0.5f; } }

    public override float MovementScalar { get { return -0.3f; } }

    public override void Create(float growScalar, float movementScalar, float lifeTimeScalar)
    {
        base.Create(growScalar, movementScalar, lifeTimeScalar);

        movespeed = 3f;
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.gravityScale = 0;

        coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.2f;
        coll.isTrigger = true;

        anim = gameObject.AddComponent<Animator>();
        sprRender = gameObject.AddComponent<SpriteRenderer>();
        gameObject.tag = "ASpell"; 

        sprRender.sprite = Resources.LoadAll<Sprite>("Sprites/ASpell")[0];
        sprRender.sortingLayerName = "Spells";
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("ACs/FireballAOC");
        anim.runtimeAnimatorController = animAOC;
    }

    public override void Update()
    {
        if (!created)
            return;

        rb2d.velocity = movespeed * direction.normalized;
    }
}
