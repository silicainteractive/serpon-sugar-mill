﻿using UnityEngine;
using System.Collections;
using System;

public class YSpell : AbstractSpell
{
    CircleCollider2D coll;

    public override float GrowScalar { get { return -0.3f; } }

    public override float LifeTimeScalar { get { return 1f; } }

    public override float MovementScalar { get { return 0.5f; } }

    public override void Create(float growScalar, float movementScalar, float lifeTimeScalar)
    {
        if (growScalar > 2.5f)
            growScalar = 2.5f;

        base.Create(growScalar, movementScalar, lifeTimeScalar);

        //movespeed = 3f;
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.gravityScale = 0;

        coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.8f;
        coll.isTrigger = false;

        sprRender = gameObject.AddComponent<SpriteRenderer>();
        gameObject.tag = "YSpell";

        sprRender.sprite = Resources.Load<Sprite>("Sprites/YSpell");
        sprRender.sortingLayerName = "Spells";

        Destroy(gameObject.GetComponent<KillScript>());
    }

    public override void Update()
    {
        if (!created) return;

        transform.position = mySpellSystem.transform.position;
    }
}