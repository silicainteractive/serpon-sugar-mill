﻿using UnityEngine;
using System.Collections;
using System;

//THUNDERBOLT WHOEHOE PIKACHUUUU
public class BSpell : AbstractSpell
{
    Animator anim;
    CircleCollider2D coll;

    public override float GrowScalar { get { return 0.5f; } }

    public override float LifeTimeScalar { get { return -0.3f; } }

    public override float MovementScalar { get { return 1f; } }

    public override void Create(float growScalar, float movementScalar, float lifeTimeScalar)
    {
        base.Create(growScalar, movementScalar, lifeTimeScalar);

        movespeed = 4f;
        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;

        coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.2f;
        coll.sharedMaterial = Resources.Load<PhysicsMaterial2D>("PhysicsMaterials/Bouncy");

        anim = gameObject.AddComponent<Animator>();
        sprRender = gameObject.AddComponent<SpriteRenderer>();
        
        gameObject.tag = "BSpell"; //don't forhet to actual declare!! [Nico] Do we actually use this? [Nico] Apparently, I do.

        sprRender.sprite = Resources.LoadAll<Sprite>("Sprites/BSpell")[0];
        sprRender.sortingLayerName = "Spells";
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("ACs/ThunderboltAOC");
        anim.runtimeAnimatorController = animAOC;
        rb2d.velocity = movespeed*direction.normalized;
    }

    public override void Update()
    {
        if (!created)
            return;

        rb2d.velocity = new Vector2(movespeed*direction.normalized.x,rb2d.velocity.y);
    }
}