﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

//Beam
public class XSpell : AbstractSpell
{
    int amount;
    GameObject playerRef;

    public override float GrowScalar { get { return 0.5f; } }

    public override float LifeTimeScalar { get { return 1f; } }

    public override float MovementScalar { get { return -0.3f; } }

    public override void Create(float growScalar, float movementScalar, float lifeTimeScalar)
    {
        Destroy(gameObject.GetComponent<KillScript>());

        amount = Random.Range(3, 5);

        playerRef = FindObjectsOfType<InputManager>().Where(i => i.playerID == ownerId).Select(i => i.gameObject).FirstOrDefault();

        for (int i = 0; i < amount; i++)
        {
            SpawnOneSpell(growScalar, movementScalar, lifeTimeScalar, i);
        }


        rb2d = gameObject.AddComponent<Rigidbody2D>();
        rb2d.gravityScale = 0f;


        movespeed = 0f;
    }

    public override void Update()
    {
    }

    public void SpawnOneSpell(float growScalar, float movementScalar, float lifeTimeScalar, int i)
    {
        var tempSpell = new GameObject();
        tempSpell.transform.position = playerRef.transform.position;

        var tempScript = tempSpell.AddComponent<MiniXSpell>();
        tempScript.mySpellSystem = playerRef.GetComponent<SpellSystem>();
        var killScript = tempSpell.AddComponent<KillScript>();
        killScript.ownerId = tempScript.ownerId = playerRef.transform.GetComponent<InputManager>().playerID;
        var offset = 0.2f * i - 0.2f*amount/2;
        if(Mathf.Abs(direction.x - direction.y) < 0.1f)
        {
            tempScript.direction = new Vector2(direction.x + offset, direction.y - offset);
        }
        else
            tempScript.direction = new Vector2(direction.x - offset, direction.y - offset);


        var angle = Mathf.Atan2(tempScript.direction.y, tempScript.direction.x) * Mathf.Rad2Deg;
        tempSpell.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        tempScript.Create(growScalar, movementScalar, lifeTimeScalar);
        Physics2D.IgnoreCollision(tempSpell.GetComponent<Collider2D>(), playerRef.transform.GetComponent<Collider2D>());

        Destroy(gameObject);
    }
}
