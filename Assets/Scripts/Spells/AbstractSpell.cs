﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

abstract public class AbstractSpell : MonoBehaviour
{
    protected SpriteRenderer sprRender;
    protected Rigidbody2D rb2d;

    public float movespeed;
    public Vector3 direction;
    public SpellSystem mySpellSystem; //Only to get the position, sorry maybe I find a better way tomorrow.

    public List<AbstractSpell> modifiers;
    public int ownerId;

    public abstract float GrowScalar { get; }
    public abstract float MovementScalar { get; }
    public abstract float LifeTimeScalar { get; }

    protected bool created = false;

    public virtual void Create(float growScalar =1, float movementScalar=1, float lifeTimeScalar=1)
    {
        gameObject.transform.localScale *= growScalar;
        movespeed *= movementScalar;

        var destroyAfterLifetime = gameObject.AddComponent<DestoryAfterLifeTime>();
        destroyAfterLifetime.lifeTime = 1.5f * lifeTimeScalar;

        //fix orientation
        created = true;
    }

    public void Modify(Queue<Type> queue) {
        if (queue.Count == 0) { // if no extra modifiers 
            Create();
            return;
        };

        modifiers = new List<AbstractSpell>();

        var tempobj = new GameObject();
        foreach (var type in queue) {
            modifiers.Add((AbstractSpell)tempobj.AddComponent(type));
        }

        var growthScalar = 0f;
        var movementScalar = 0f;
        var lifeTimeScalar = 0f;

        foreach (var item in modifiers)
        {
            growthScalar += item.GrowScalar;
            movementScalar += item.MovementScalar;
            lifeTimeScalar += item.LifeTimeScalar;
        }

        growthScalar = Mathf.Clamp(growthScalar, 1.5f, 5);
        movementScalar = Mathf.Clamp(movementScalar, 1, 3);
        lifeTimeScalar = Mathf.Clamp(lifeTimeScalar, 1, 3);

        DestroyImmediate(tempobj);
        Create(growthScalar, movementScalar, lifeTimeScalar);
    }

    public static void CastSpellOfType(Type spellType, Queue<Type> queue, Vector3 target, Transform otherTransform, SpellSystem spellSystem) {
        var pos = otherTransform.position;
        var tempSpell = new GameObject();
        tempSpell.transform.position = pos; //this should be player pos??

        var tempScript = (AbstractSpell)tempSpell.AddComponent(spellType);
        tempScript.mySpellSystem = spellSystem;
        var killScript= tempSpell.AddComponent<KillScript>();
        killScript.ownerId = tempScript.ownerId = otherTransform.GetComponent<InputManager>().playerID;
        tempScript.direction = target;
        var angle = Mathf.Atan2(target.y, target.x) * Mathf.Rad2Deg;
        tempSpell.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        tempScript.Modify(queue);
        Physics2D.IgnoreCollision(tempSpell.GetComponent<Collider2D>(), otherTransform.GetComponent<Collider2D>());
    }

    public abstract void Update();
}
