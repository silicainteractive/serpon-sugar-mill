﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RoundWatcher : MonoBehaviour {
    private int roundKills = 0;

    public void Awake() {
        roundKills = 0;
        ScoreBoard.Instance.OnKill += (s, e) => {
            roundKills++;
        };
    }

    private void Update () {
        if (ScoreBoard.Instance.debugMode) return;
        if (roundKills < ScoreBoard.Instance.numberOfPlayers - 1) return;
        ScoreBoard.Instance.SaveScores();
        SceneManager.LoadScene(3);
    }
}
